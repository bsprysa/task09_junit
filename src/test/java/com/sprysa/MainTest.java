package com.sprysa;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.Mockito.when;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MainTest {

  @InjectMocks
  Main main;

  @Mock
  MainInterface mainMock;

  @BeforeClass
  public static void beforeClass() {
    System.out.println("Output before class");
  }

  @Before
  public void beforeEach() {
    System.out.println("Output before each method");
  }

  @AfterClass
  public static void afterClass() {
    System.out.println("Output after class");
  }

  @Test
  public void testAdd() {
    Main main = new Main();
    int sum = main.add(3, 5);
    assertEquals(sum, 8);
    System.out.println("Output from testAdd()");
  }

  @Test
  public void testMinus() {
    Main main = new Main();
    int minus = main.minus(5, 1);
    assertSame(minus, 4);
    System.out.println("Output from testMinus()");
  }

  @Test
  public void testMinus2() {
    Main main = new Main();
    int minus = main.minus(5, 1);
    assumeTrue(minus == 4);
    System.out.println("Output from testMinus2()");
  }

  @Test
  public void testMinusMockito() {
//    when(mainMock.minus(6, 3)).thenReturn(3);
//    assertEquals(main.minus(6, 3), 3);
//    verify(mainMock).minus(6, 3);

    MainInterface mInt = Mockito.mock(MainInterface.class);
    Main mainIn = new Main(mInt);
    when(mInt.minus(6, 3)).thenReturn(3);
    assertEquals(mainIn.minus(6, 3), 3);
    System.out.println("Output from testMinusMockito()");
  }


}
