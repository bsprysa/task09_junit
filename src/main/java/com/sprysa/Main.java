package com.sprysa;

public class Main implements MainInterface {

  private MainInterface mInt;

  public Main() {
  }

  public Main(MainInterface mInt) {
    this.mInt = mInt;
  }

  @Override
  public int add(int a, int b) {
    return a + b;
  }

  @Override
  public int minus(int a, int b) {
    return a - b;
  }

  public boolean isEquel(int a, int b) {
    return mInt.isEquel(a, b);
  }

}
