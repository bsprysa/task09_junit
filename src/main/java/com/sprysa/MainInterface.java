package com.sprysa;

public interface MainInterface {

  public int add(int a, int b);
  public int minus(int a, int b);
  public boolean isEquel(int a, int b);

}
